Running system tests

Working with Cisco's remote docker artifactory repository:
 In order to use you must place a file called 'dockercfg' in the docker-system-test folder.
 To generate this on a 'unix like' system type the following from inside docker-system-test:
    curl -u USERNAME "https://engci-docker.cisco.com:5003/v1/auth" > dockercfg
 substituting USERNAME for your cisco username and providing your cisco password when asked.

To use:
  type 'vagrant up' - This will download and start the docker service
  type 'vagrant ssh' - This will login to the docker service
  type 'cd /vagrant; crane lift' - This will start ejabberd, xmi, oracle and TMS. Exposing the following ports:
    ejabberd on ports 5222 and 5275
    XMI on port 8080
    oracle on port 1521
    TMS on ports 5410 and 5411
    UpmMock on port 6040
  All hosts accessible through localhost
  Run your system test


  ### Troubleshooting ###
  If you experience a connection problem during the crane lift execution, to retry run:
    crane kill
    crane rm
    sudo service docker restart
    sudo chmod a+rw /var/run/docker.sock
    crane lift