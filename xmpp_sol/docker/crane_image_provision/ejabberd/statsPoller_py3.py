import urllib.request
import subprocess

def get_stats(ejabberdctl_path, connected_cmd, registered_cmd, uptime_cmd, connectedNode_cmd, status_cmd):
    connected = subprocess.getoutput(ejabberdctl_path + " " + connected_cmd)
    registered = subprocess.getoutput(ejabberdctl_path + " " + registered_cmd)
    uptime = subprocess.getoutput(ejabberdctl_path + " " + uptime_cmd)
    connectedNode = subprocess.getoutput(ejabberdctl_path + " " + connectedNode_cmd)
    status = subprocess.getoutput(ejabberdctl_path + " " + status_cmd)
    return (connected, registered, uptime, connectedNode, status)

def push_stats(monitoring_url, data):
    req = urllib.request.Request(monitoring_url)
    req.add_header('Content-Type', 'application/json')

    res = urllib.request.urlopen(req, data.encode('utf-8'))
    return res.getcode()
