import sys
from struct import *
from urllib2 import urlopen
from urllib2 import URLError
import codecs

def set_stdin() :
    return codecs.getwriter('utf-8')(sys.stdin)

def send_req(url, source,data=None):
    try:
        if data is None:
            urlopen(url)
        else:
            data = data.encode('utf-8') 
            urlopen(url,data=data)
    except URLError as e:
        if hasattr(e, 'reason'):
            error = source + " Failed: "+str(e.reason)
        elif hasattr(e, 'code'):
            error = source + " Failed: "+str(e.code)
        else:
            error = "Authorisation Failed"
        return (False, error)
    return (True, None)
