import time
import argparse
import json
import logging

logging.basicConfig(filename = '/opt/ejabberd/logs/statsPoller.log', level=logging.INFO)

def poll_stats():
    connected, registered, uptime, connectedNode, status = get_stats(ejabberdctl_path, connected_cmd, registered_cmd, uptime_cmd, connectedNode_cmd, status_cmd)
    data = json.dumps({'stats': {'onlineusers':connected.strip(),'registeredusers':registered.strip(),'uptimeseconds':uptime.strip(),'onlineusersnode':connectedNode.strip(),'status':status.strip()}})
    print(data)
    try:
        code = push_stats(monitoring_url, data)
        if code < 200 or code > 299:
            logging.error("Received an unsuccessful status code %d" % code)
    except Exception as e:
        logging.error("Error posting monitoring stats.", e)

    return True

parser = argparse.ArgumentParser()
parser.add_argument("--path", help="The path to ejabberdctl.")
parser.add_argument("--host", help="The XMI host to send stats to.")
parser.add_argument("--port", help="The XMI port to send stats to.")
parser.add_argument("--node", help="The node name to use in monitoring for the ejabberd instance.")
parser.add_argument("--poll_duration", help="The polling duration (in seconds) of stat calls.")
parser.add_argument("--python_version", help="The python base version supported by the system (currently allowed values are 2 or 3)")
args = parser.parse_args()
path = args.path
host = args.host
port = args.port
node = args.node
poll_duration = int(args.poll_duration)

if args.python_version == "2":
    get_stats = getattr(__import__("statsPoller_py2"), "get_stats")
    push_stats = getattr(__import__("statsPoller_py2"), "push_stats")
else:
    get_stats = getattr(__import__("statsPoller_py3"), "get_stats")
    push_stats = getattr(__import__("statsPoller_py3"), "push_stats")


ejabberdctl_path = path + "/ejabberdctl"
connected_cmd = "stats onlineusers"
registered_cmd = "stats registeredusers"
uptime_cmd = "stats uptimeseconds"
connectedNode_cmd = "stats onlineusersnode"
status_cmd = "status"
monitoring_url = "http://" + host + ":" + port + "/monitoring/" + node

logging.info("Starting polling of ejabberd monitoring stats.")
while True:
    poll_stats()
    time.sleep(poll_duration)