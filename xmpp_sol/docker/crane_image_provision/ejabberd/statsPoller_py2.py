from urllib2 import urlopen
from urllib2 import Request
import subprocess

def get_stats(ejabberdctl_path, connected_cmd, registered_cmd, uptime_cmd, connectedNode_cmd, status_cmd):
    p = subprocess.Popen([ejabberdctl_path, connected_cmd], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    connected, err = p.communicate()

    p = subprocess.Popen([ejabberdctl_path, registered_cmd], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    registered, err = p.communicate()

    p = subprocess.Popen([ejabberdctl_path, uptime_cmd], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    uptime, err = p.communicate()

    p = subprocess.Popen([ejabberdctl_path, connectedNode_cmd], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    connectedNode, err = p.communicate()

    p = subprocess.Popen([ejabberdctl_path, status_cmd], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    status, err = p.communicate()
    return (connected, registered, uptime, connectedNode, status)

def push_stats(monitoring_url, data):
    req = Request(monitoring_url, data.encode('utf-8'), {'Content-Type': 'application/json'})
    res = urlopen(req)
    return res.getcode()