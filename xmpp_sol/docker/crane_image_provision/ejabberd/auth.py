import sys
from struct import *
import argparse
import logging
import logging.handlers

sys.stderr = open('/opt/ejabberd/logs/auth_err.log', 'w')

try:
    from cloghandler import ConcurrentRotatingFileHandler as RFhandler
except ImportError:
    #Next 2 lines are optional:  issue a warning to the user
    from warnings import warn
    warn("ConcurrentLogHandler package not installed.  Using builtin log handler")
    from logging.handlers import RotatingFileHandler as RFhandler

LOG_FILENAME = '/opt/ejabberd/logs/auth.log'

# Set up a specific logger with the desired output level
ext_logger = logging.getLogger('Ejabberd_ExtAuth_Logger')
ext_logger.setLevel(logging.INFO)

# Add the log message handler to the logger
handler = RFhandler(
              LOG_FILENAME, maxBytes=2000000, backupCount=5)

ext_logger.addHandler(handler)
	
def from_ejabberd():
    ext_logger.debug("Waiting for input")
    input_length = sys.stdin.read(2)
    ext_logger.debug("Got "+str(input_length)+"bytes")
    (size,) = unpack('>h', input_length)
    ext_logger.debug("Unpacked to "+str(size)+"bytes")
    read = sys.stdin.read(size).decode('utf-8',"ignore").split(':')
    ext_logger.debug("Got :"+str(read))
    return read

def to_ejabberd(bool):
    answer = 0
    if bool:
        answer = 1
    token = pack('>hh', 2, answer)
    sys.stdout.write(token.decode('ascii'))
    sys.stdout.flush()
    return True

def setpass(username, server, password):
    ext_logger.info("setPass("+username+", "+server+", ######)")
    ext_logger.error("Setpass Failed")
    return False

def auth(username, server, password):
    return True
    ext_logger.info("auth("+username+", "+server+", ######)")
    username = username.strip()
    password = password.strip()
    data = 'username='+username+'@'+server+'&password='+password
    status, err = send_req("http://"+tmsendpoint+"/tms/ott/account/login/","Authorisation",data)
    if err is not None:
        ext_logger.error(err)
    return True

def isuser(username, server):
    ext_logger.info("isUser("+username+", "+server+")")
    status, err = send_req("http://"+tmsendpoint+"/user/"+username+"@"+server, "Isuser")
    if err is not None:
        ext_logger.error(err)
    return status

parser = argparse.ArgumentParser()
parser.add_argument("--tms", help="TMS endpoint in the form HOSTNAME:PORT")
parser.add_argument("--python_version", help="The python base version supported by the system (currently allowed values are 2 or 3)")
args = parser.parse_args()
tmsendpoint = args.tms

if args.python_version == "2":
    send_req = getattr(__import__("auth_py2"), "send_req")
    set_stdin = getattr(__import__("auth_py2"), "set_stdin")
else:
    send_req = getattr(__import__("auth_py3"), "send_req")
    set_stdin = getattr(__import__("auth_py3"), "set_stdin")

sys.stdin = set_stdin()

while True:
    data = from_ejabberd()
    success = False
    if data[0] == "auth":
        success = auth(data[1], data[2], data[3])
    elif data[0] == "isuser":
        success = isuser(data[1], data[2])
    elif data[0] == "setpass":
        success = setpass(data[1], data[2], data[3])
    to_ejabberd(success)
