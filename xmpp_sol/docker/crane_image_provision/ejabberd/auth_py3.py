import sys
from struct import *
import urllib.request
from urllib.error import URLError
import codecs

def set_stdin() :
    return codecs.getwriter('utf-8')(sys.stdin.detach())

def send_req(url, source, data=None):
    try:
        if data is None:
            req = urllib.request.Request(url)
        else:
            data = data.encode('utf-8')
            req = urllib.request.Request(url,data=data)
        urllib.request.urlopen(req)
    except URLError as e:
        if hasattr(e, 'reason'):
            error = source + " Failed: "+str(e.reason)
        elif hasattr(e, 'code'):
            error = source + " Failed: "+str(e.code)
        else:
            error = "Authorisation Failed"
        return (False, error)
    return (True, None)
